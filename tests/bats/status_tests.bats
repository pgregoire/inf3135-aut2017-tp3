#!/usr/bin/env bats

@test 'help' {
  run ./tp3 -h
  [ X0 = X"${status}" ]
}

@test 'too many arguments' {
  run ./tp3 blob
  [ X1 = X"${status}" ]
}

@test 'invalid argument' {
  run ./tp3 -H
  [ X1 = X"${status}" ]
}
