# La grille de jeu

Afin d'abstraire le type d'interface utilisé par le joueur, on traite
l'espace de jeu comme une grille avec séparateurs.

## Les éléments de la grille

```
+-+-------+-+-------+-+
|a|   b   |c|   d   |e|
+-+-------+-+-------+-+
| |       | |       | |
| |       | |       | |
|f|   A   |g|   B   |h|
| |       | |       | |
| |       | |       | |
+-+-------+-+-------+-+
|i|   j   |k|   l   |m|
+-+-------+-+-------+-+
| |       | |       | |
| |       | |       | |
|n|   C   |o|   D   |p|
| |       | |       | |
| |       | |       | |
+-+-------+-+-------+-+
|q|   r   |s|   t   |u|
+-+-------+-+-------+-+
```

### Les cellules de positionnement

Une cellule de positionnement est un espace capable d'accueillir le
caractère ou un *donut*. Ci-haut, on les retrouvent identifiés par
une lettre majuscule.

### Les séparateurs

Les séparateurs entourent chacune des cellules de positionnement. S'ils
sont remplis, ils agissent comme mur, plancher ou plafond.

Lorsqu'un segment de séparateur est partagé par plusieurs cellules de
positionnement, il suffit que l'un deux remplissent le séparateur pour
que le segment soit remplis.


## Représentation

Pour chaque cellule, un tupple de 4 entiers indiquant si les
séparateurs sont remplis. Les entiers sont spécifiés dans l'ordre
suivant: haut, droite, bas gauche

Notons que les éléments `{c, g, k, o, s}` sont partagés. Si l'une
des cellules qui la partage définit un séparateur, l'élément est
remplis.


## Entreposage

Chaque niveau est entreposé dans un fichier au format JSON

### Exemple

```
{ "dimensions": [2,2],
  "separators": [
	[1,0,1,1], [1,1,0,0],
	[1,0,1,1], [0,1,1,0]
  ],
  "donuts": [0]
}
```

```
+-+-------+-+-------+-+
|x|xxxxxxx|x|xxxxxxx|x|
+-+-------+-+-------+-+
|x|       | |       |x|
|x| DDDDD | |       |x|
|x| D   D | |       |x|
|x| DDDDD | |       |x|
|x|       | |       |x|
+-+-------+-+-------+-+
|x|xxxxxxx|x|       |x|
+-+-------+-+-------+-+
|x|       | |       |x|
|x|       | |       |x|
|x|       | |       |x|
|x|       | |       |x|
|x|       | |       |x|
+-+-------+-+-------+-+
|x|xxxxxxx|x|xxxxxxx|x|
+-+-------+-+-------+-+
```
