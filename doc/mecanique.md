# La mécanique de jeu

Ce déplacement présente la mécanique du jeu TP3.

## La grille de jeu

L'espace de jeu est une grille dans laquelle le joueur peut déplacer
le caractère. Entre chaque case se situe une marge de la largeur d'un
mur. Cette marge peut être remplis ou vide; selon la présence d'un
obstacle.

### Exemple de grille de jeu

Le diagramme suivant montre une grille de jeu fermé. On notera que
la grille comporte un case inaccessible (C).

```
            +-+--------+-+--------+-+--------+-+
            |x|xxxxxxxx|x|xxxxxxxx|x|xxxxxxxx|x|
            +-+--------+-+--------+-+--------+-+
            |x|        | |        |x|        |x|
            |x|        | |        |x|        |x|
            |x|        | |        |x|        |x|
            |x|   A    | |   B    |x|   C    |x|
            |x|        | |        |x|        |x|
            |x|        | |        |x|        |x|
            +-+--------+-+--------+-+--------+-+
            |x|        |x|xxxxxxxx|x|xxxxxxxx|x|
            +-+--------+-+--------+-+--------+-+
            |x|        | |        | |        |x|
            |x|        | |        | |        |x|
            |x|        | |        | |        |x|
            |x|   D    | |   E    | |   F    |x|
            |x|        | |        | |        |x|
            |x|        | |        | |        |x|
            +-+--------+-+--------+-+--------+-+
            |x|xxxxxxxx|x|xxxxxxxx|x|xxxxxxxx|x|
            +-+--------+-+--------+-+--------+-+
```

## Les déplacements

Le joueur peut déplacer le caractère dans la grille en utilisant les
touches de direction et de saut. Pour simplifier l'implémentation de
la mécanique de saut, les touches de direction possèdent deux (2)
fonctions:

* contrôler l'orientation du caractère;
* déplacer le caractère au sol.

### L'orientation

Spécifiquement, l'orientation initiale du caractère est l'avant (le
caractère est orienté vers le joueur). En pressant les touches de
direction peut ré-orienter le caractère. Comme nous verrons plus tard,
l'orientation est particulièrement importante dans la manière dont le
jeu contrôle les déplacements par saut.

### Le déplacement au sol

Lorsque le joueur presse la touche de direction vers la gauche ou la
droite et que le caractère est orienté dans la même direction, le
jeu tente de déplacer le caractère vers la case situé dans la
direction correspondante.

Avant un déplacement, le jeu détermine s'il existe un mur entre la case
courante et la destination. Si c'est le cas, aucun déplacement n'est
effectué. S'il n'existe pas de mur, le caractère est déplacé vers la
case cible.

Après un déplacement, le jeu vérifie si la case sur laquelle le joueur
se situe possède un plancher. S'il n'y en a pas, le caractère tombe à la
case du niveau inférieur. S'il existe un plancher, le déplacement est
complet. Autrement, le joueur tombe de nouveau au niveau inférieur;
jusqu'à ce qu'il y ait un plancher.

### Le déplacement par saut

Lorsque le joueur presse la touche de saut, le jeu tente d'effectuer
un déplacement par saut du caractère; selon l'orientation du
caractère.

Si le caractère est orienté vers la gauche ou la droite, le joueur
peut déplacer le caractère à la case du niveau supérieur et vers la
gauche ou droite (selon l'orientation). Si le caractère est orienté
vers l'avant, le caractère fait un saut sur place.

Avant d'effectuer le saut, le jeu détermine s'il existe un plafond
au-dessus de la grille où le caractère est situé. S'il en existe un,
aucun saut n'est effectué. Également, si le caractère est orienté
vers la gauche ou la droite, après avoir vérifié qu'il n'existe aucun
plafond, le jeu détermine s'il existe un mur entre le case du niveau
supérieur immédiat et la case de gauche ou droite (selon l'orientation
du caractère).

### Exemple

```
            +-+--------+-+--------+-+--------+-+
            |x|xxxxxxxx|x|xxxxxxxx|x|xxxxxxxx|x|
            +-+--------+-+--------+-+--------+-+
            |x|        | |        |x|        |x|
            |x|        | |        |x|        |x|
            |x|        | |        |x|        |x|
            |x|   A    | |   B    |x|   C    |x|
            |x|        | |        |x|        |x|
            |x|        | |        |x|        |x|
            +-+--------+-+--------+-+--------+-+
            |x|xxxxxxxx|x|        |x|xxxxxxxx|x|
            +-+--------+-+--------+-+--------+-+
            |x|        | |        | |        |x|
            |x|        | |        | |        |x|
            |x|        | |        | |        |x|
            |x|   D    | |   E    | |   F    |x|
            |x|        | |        | |        |x|
            |x|        | |        | |        |x|
            +-+--------+-+--------+-+--------+-+
            |x|xxxxxxxx|x|xxxxxxxx|x|xxxxxxxx|x|
            +-+--------+-+--------+-+--------+-+
```

La grille de jeu ci-haut permet les déplacements suivants:
`{ (A, B), (B, E), (E, B), (E, D), (D, E), (E, F), (F, E), (E, A) }`.

### Matrice d'actions

```
+----------------------+-------------+-------------+-------------+
| touche \ orientation |    avant    |   gauche    |   droite    |
+======================+=============+=============+=============+
| avant                | aucune      | orientation | orientation |
| gauche               | orientation | déplacement | orientation |
| droite               | orientation | orientation | déplacement |
| saut                 | saut        | déplacement | déplacement |
+----------------------+-------------+-------------+-------------+
```
