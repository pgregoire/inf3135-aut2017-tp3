# L'implémentation du TP3

## Le critère de décomposition

Pour l'implémentation du jeu TP3, le critère de décomposition adopté
est celui présenté par Parnas dans
[On the criteria to be used in decomposing systems into modules][1]:
masquer les détails d'implémentation.

Effectuant le travail seul, nous croyons également que ce critère peut
être utilisé pour faciliter la collaboration; en permettant à chacun
de travailler le plus indépendamment possible.

## Les éléments susceptibles de changer

Pour commencer, nous avons analyser les exigences du jeu et déterminer
que les éléments suivants sont susceptibles de changer (sans ordre
particulier):

* la mécanique de jeu;
* l'aspect du caractère;
* l'aspect des items à ramasser;
* l'interface de jeu;
* la librairie d'interfacage.

## Les modules

Une fois les éléments susceptibles de changer identifiés, chacun d'eux
a été assigné son propre module d'encapsulation.

### Le module `game`

Le module `game` est responsable de la dynamique de jeu et de la
transition du jeu d'un état à un autre. Son secret est les différents
états de jeu, leur représentation et l'algorithme utilisé pour contrôler
les transitions d'un état à un autre.

#### Le sous-module `menu`

Le sous-module `menu` est responsable de l'implémentation de l'état de
jeu dans lequel le joueur peut choisir de jouer ou quitter le jeu.

#### Le sous-module `play`

Le sous-module `play` est responsable de l'implémentation de l'état de
jeu dans lequel le joueur joue.

##### Le sous-module `map`

Le sous-module `map` est responsable de l'affichage des cartes
de niveau. Son secret est la structure de données utilisée pour
représenter les cartes de niveau.

##### Le sous-module `character`

Le sous-module `character` est responsable de l'affichage du
caractère. Son secret est l'algorithme utilisé pour représenter
et afficher le caractère.

##### Le sous-module `donut`

Le sous-module `donut` est responsable de l'affichage des items
à collecter. Son secret est l'algorithme utilisé pour représenter
et afficher le caractère.

### Le module `ui`

Le module `ui` est responsable de l'affichage du jeu à l'écran.
Son secret est le type d'interface utilisé.

#### Le module `gfx`

Le module `gfx` fournit des routines de manipulations graphiques.
Son secret est les algorithmes utilisés pour dessiner à l'écran.

### Le module `main`

Le module `main` est responsable de l'initialisation et du
bon déroulement du programme.

[1]: https://dl.acm.org/citation.cfm?doid=361598.361623
