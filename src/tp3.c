/*
 * Copyright 2017, Philippe Gregoire <pg@pgregoire.xyz>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* ================================================================== */

#define _XOPEN_SOURCE	700

#include <getopt.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>

#include "game.h"
#include "ui.h"

/* ------------------------------------------------------------------ */

#define O_DEBUG		0x01

static int init_tp3(int flags)
{
	if (0 != (O_DEBUG & flags)) {
		setenv("TP3_DEBUG", "1", 1);
	}

	if (0 == init_ui()) {
		return init_game();
	}

	return -1;
}

static void exit_tp3(void)
{
	exit_game();
	exit_ui();
}

/* ------------------------------------------------------------------ */

static void usage(const char* progname, FILE* fp)
{
	fprintf(fp, "usage: %s [-dh]\n", progname);
}

int main(int argc, char* const* argv)
{
	int o;
	int o_flags;

	o_flags = 0;

	while (-1 != (o = getopt(argc, argv, "dh"))) {
		switch (o) {
		case 'd':
			o_flags |= O_DEBUG;
			break;
		case 'h':
			usage(basename(argv[0]), stdout);
			return 0;
		default:
			usage(basename(argv[0]), stderr);
			return 1;
		}
	}

	if (0 != (argc - optind)) {
		usage(basename(argv[0]), stderr);
		return 1;
	}


	o = 1;
	if (0 == init_tp3(o_flags)) {
		o = game_run();
	}

	exit_tp3();

	return o;
}

/* ================================================================== */
