/*
 * Copyright 2017, Philippe Gregoire <pg@pgregoire.xyz>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* ================================================================== */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

/* ------------------------------------------------------------------ */

static void vxprintf(const char* fmt, va_list vl, FILE* fp)
{
	int e;
	size_t i;

	e = errno;
	fflush(stdout);
	vfprintf(fp, fmt, vl);

	i = (strlen(fmt) - 1);
	if (':' == fmt[i]) {
		fprintf(fp, " %s\n", strerror(e));
	}

	errno = e;
}

/* ------------------------------------------------------------------ */

void vgprintf(const char* fmt, va_list vl)
{
	char* s;

	s = getenv("TP3_DEBUG");
	if ((0 != s) && (0 != strcmp("", s))) {
		vxprintf(fmt, vl, stdout);
	}
}

void gprintf(const char* fmt, ...)
{
	va_list vl;

	va_start(vl, fmt);
	vgprintf(fmt, vl);
	va_end(vl);
}

/* ------------------------------------------------------------------ */

void veprintf(const char* fmt, va_list vl)
{
	vxprintf(fmt, vl, stderr);
}

void eprintf(const char* fmt, ...)
{
	va_list vl;

	va_start(vl, fmt);
	veprintf(fmt, vl);
	va_end(vl);
}

void xeprintf(int xcode, const char* fmt, ...)
{
	va_list vl;

	va_start(vl, fmt);
	veprintf(fmt, vl);
	va_end(vl);

	exit(xcode);
}

/* ================================================================== */
