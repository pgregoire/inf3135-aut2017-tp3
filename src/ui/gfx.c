/*
 * Copyright 2017, Philippe Gregoire <pg@pgregoire.xyz>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* ================================================================== */

#include <sys/time.h>

#include <assert.h>
#include <errno.h>

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_render.h>
#include <SDL_ttf.h>

#include "ui.h"
#include "../utils.h"

/* ------------------------------------------------------------------ */

#define WINDOW_HEIGHT	640
#define WINDOW_WIDTH	800
#define SEP_MAXLEN	20

#define WINDOW_FLAGS	(SDL_WINDOW_HIDDEN)
#define RENDERER_FLAGS	(SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC)

/* ------------------------------------------------------------------ */

struct imgpriv {
	size_t		fwidth;
	size_t		fheight;
	SDL_Texture*	texture;
};

static size_t seplen;
static size_t cellxlen;
static size_t cellylen;

static SDL_Renderer* rendrr;
static TTF_Font* font;

/* ------------------------------------------------------------------ */

static size_t mapcoord(size_t pt, size_t ptlen)
{
	size_t c;
	size_t s;

	s = ((pt / 2) + (pt & 1));
	c = (pt - s);

	return ((s * seplen) + (c * ptlen));
}

/*
 * The corner of a matrix's cell depends on its position.
 * For example, the corner at (0,0) is at (0,0). The
 * cell at (1,1), however, is at (seplen, seplen).
 *
 * This routine returns the rectangle matching (x, y).
 */
static void getrect(size_t x, size_t y, SDL_Rect* rect)
{
	assert(0 != rect);

	rect->x = mapcoord(x, cellxlen);
	rect->y = mapcoord(y, cellylen);

	switch ((x & 1) | ((y & 1) << 1)) {
	case 0: /* corner */
		rect->h = seplen;
		rect->w = seplen;
		break;
	case 1: /* horizontal separator */
		rect->h = seplen;
		rect->w = cellxlen;
		break;
	case 2: /* vertical separator */
		rect->h = cellylen;
		rect->w = seplen;
		break;
	default: /* regular cell */
		rect->h = cellylen;
		rect->w = cellxlen;
		break;
	}
}

static int draw_rect(size_t x, size_t y)
{
	assert(0 != rendrr);

	SDL_Rect r;

	getrect(x, y, &r);
	if (0 != SDL_RenderFillRect(rendrr, &r)) {
		xeprintf(1, "Failed to draw rectangle: %s\n", SDL_GetError());
	}

	return 0;
}

static int draw_mxcell(size_t x, size_t y, const struct screen* scr)
{
	assert((0 != scr) && (0 != scr->matrix));

	size_t mi;

	gprintf("ui/gfx: Drawing cell (%lu, %lu)\n", x, y);

	mi = screen_mindexof(x, y, scr->width);

	if (0 != scr->matrix[mi]) {
		SDL_SetRenderDrawColor(rendrr, 64, 128, 128, 255);
	} else {
		SDL_SetRenderDrawColor(rendrr, -1, -1, -1, 255);
	}

	if (0 != draw_rect(x, y)) {
		return -1;
	}

	return 0;
}

/* Compute the length of separators and cells. */
static void setlengths(size_t w, size_t h)
{
	size_t nseps;

	seplen = (WINDOW_WIDTH / w);
	seplen = (seplen > SEP_MAXLEN) ? SEP_MAXLEN : seplen;

	nseps = (w / 2) + 1;
	cellxlen = (WINDOW_WIDTH - (seplen * nseps));
	cellxlen = (cellxlen / (w - nseps));

	nseps = (h / 2) + 1;
	cellylen = (WINDOW_HEIGHT - (seplen * nseps));
	cellylen = (cellylen / (h - nseps));
}

static int gfx_load(const struct screen* scr)
{
	assert((0 != scr) && (0 != scr->matrix));

	size_t h;
	size_t w;
	size_t x;
	size_t y;


	gprintf("ui/gfx: Loading screen\n");

	h = screen_lenfor(scr->height);
	w = screen_lenfor(scr->width);
	setlengths(w, h);

	for (y = 0 ; y < h ; y++) {
		for (x = 0 ; x < w ; x++) {
			if (0 != draw_mxcell(x, y, scr)) {
				return -1;
			}
		}
	}

	return 0;
}

static void gfx_unload(void)
{
	/* nothing to do */
}

static int gfx_update(void)
{
	SDL_RenderPresent(rendrr);

	return 0;
}

/* ------------------------------------------------------------------ */

static size_t grid2matrix(size_t c)
{
	return ((2 * c) + 1);
}

static void map2matrix(const struct coord* orig, struct coord* map)
{
	assert((0 != orig) && (0 != map));

	map->x = grid2matrix(orig->x);
	map->y = grid2matrix(orig->y);
}

static int loadfont(void)
{
	/*
	 * TODO At the moment, we only write text for the menu,
	 *      but there should be a public function to load fonts.
	 */

#define P "src/assets/LinLibertine_M.otf"

	return -(0 == (font = TTF_OpenFont(P, 32)));
}

static int
__gfx_write(const char* str, size_t x, size_t y, size_t w, size_t h)
{
	assert((0 != str) && (0 != rendrr));

	SDL_Rect r;
	SDL_Color c;
	SDL_Surface* s;
	SDL_Texture* t;

	gprintf("Writing %s at (%lu, %lu)\n", str, x, y);

	c.r = 0; c.g = 0; c.b = 0; c.a = 255;
	if (0 == (s = TTF_RenderText_Solid(font, str, c))) {
		eprintf("Failed to render text: %s\n", TTF_GetError());
		return -1;
	}

	if (0 == (t = SDL_CreateTextureFromSurface(rendrr, s))) {
		eprintf("Failed to create texture: %s\n", SDL_GetError());
		return -1;
	}

	r.x = x; r.y = y; r.h = h; r.w = w;
	if (0 != SDL_RenderCopy(rendrr, t, 0, &r)) {
		eprintf("Failed to render text: %s\n", SDL_GetError());
		return -1;
	}

	return 0;
}

static int gfx_write(const char* str, const struct coord* coord)
{
	assert((0 != str) && (0 != coord));

	int h;
	int w;
	struct coord c;


	if ((0 == font) && (0 != loadfont())) {
		return -1;
	}

	gprintf("gfx_write: \"%s\" at (%lu,%lu)\n", str, coord->x, coord->y);


	if (0 != TTF_SizeText(font, str, &w, &h)) {
		eprintf("Failed to determine size of: %s\n", str);
		return -1;
	}

	gprintf("> size of text is %i x %i\n", w, h);
	if ((w > cellxlen) || (h > cellylen)) {
		errno = E2BIG;
		return -1;
	}

	map2matrix(coord, &c);
	gprintf("mapping to (%lu,%lu)\n", c.x, c.y);

	c.x = mapcoord(c.x, cellxlen);
	c.y = mapcoord(c.y, cellylen);

	/* center horizontally */
	c.x = (c.x + (cellxlen / 2) - (w / 2));
	/* bottom vertically */
	c.y = (c.y + cellylen - h);

	return __gfx_write(str, c.x, c.y, w, h);
}

static int
__gfx_draw(size_t frame, const struct img* img, const SDL_Rect* d)
{
	assert((0 != img) && (0 != d));

	SDL_Rect s;
	struct imgpriv* p;

	p   = img->priv;
	s.x = ((frame % img->ncols) * p->fwidth);
	s.y = ((frame / img->ncols) * p->fheight);
	s.w = p->fwidth;
	s.h = p->fheight;

	gprintf("Drawing (%u,%u)[%ux%u] to (%u,%u)[%ux%u]\n", s.x,s.y,s.w,s.h,d->x,d->y,d->w,d->h);

	SDL_SetRenderDrawColor(rendrr, -1, -1, -1, 0);
	if (0 != SDL_RenderCopy(rendrr, p->texture, &s, d)) {
		eprintf("Failed to draw image: %s\n", SDL_GetError());
		return -1;
	}

	return 0;
}

static int
gfx_draw(size_t frame, const struct img* img, const struct coord* coord)
{
	assert(0 != rendrr);

	SDL_Rect d;
	struct coord c;


	if ( (0 == img) || (0 == img->priv) || (0 == coord) ||
	     (0 == ( ((struct imgpriv*)img->priv)->texture )) )
	{
		errno = EINVAL;
		return -1;
	}

	gprintf("gfx: Drawing frame %lu\n", frame);


	map2matrix(coord, &c);
	d.x = mapcoord(c.x, cellxlen);
	d.y = mapcoord(c.y, cellylen);
	d.w = cellxlen;
	d.h = cellylen;

	return __gfx_draw(frame, img, &d);
}

static void getoff(double xoff, double yoff,
                   const struct coord* from, const struct coord* to,
                   SDL_Rect* rect)
{
	assert((0 != from) && (0 != to) && (0 != rect));

	struct coord f;
	struct coord t;

	map2matrix(from, &f);
	map2matrix(to, &t);

	rect->x = (mapcoord(t.x, cellxlen) - mapcoord(f.x, cellxlen));
	rect->x = (mapcoord(f.x, cellxlen) + (rect->x * xoff));
	rect->y = (mapcoord(t.y, cellylen) - mapcoord(f.y, cellylen));
	rect->y = (mapcoord(f.y, cellylen) + (rect->y * yoff));
}

static int gfx_drawoff(size_t frame, const struct img* img,
                       double xoff, double yoff,
                       const struct coord* from, const struct coord* to)
{
	if ( (0 == img) || (img->nimgs <= frame) ||
	     (0 == from) || (0 == to) )
	{
		errno = EINVAL;
		return -1;
	}

	gprintf("ui/gfx_drawoff: %g,%g\n", xoff, yoff);

	SDL_Rect r;

	getoff(xoff, yoff, from, to, &r);
	r.w = cellxlen;
	r.h = cellylen;

	return __gfx_draw(frame, img, &r);
}

static int gfx_clearoff(const struct img* img, double xoff, double yoff,
                        const struct coord* from, const struct coord* to)
{
	if ( (0 == img) || (0 == from) || (0 == to) ) {
		errno = EINVAL;
		return -1;
	}

	SDL_Rect r;

	getoff(xoff, yoff, from, to, &r);
	r.w = cellxlen;
	r.h = cellylen;

	SDL_SetRenderDrawColor(rendrr, -1, -1, -1, -1);
	SDL_RenderFillRect(rendrr, &r);

	return 0;
}

/* -------------------------------------------------------------------- */

static int gfx_clear(const struct coord* coord)
{
	assert(0 != coord);

	struct coord c;

	gprintf("Clearing cell (%lu,%lu)\n", coord->x, coord->y);

	map2matrix(coord, &c);

	SDL_SetRenderDrawColor(rendrr, -1, -1, -1, -1);
	draw_rect(c.x, c.y);

	return 0;
}


/* ------------------------------------------------------------------ */

static int gfx_loadimg(const char* path, size_t ncols, size_t nrows,
                       size_t nimgs, struct img* img)
{
	assert(0 != rendrr);

	SDL_Surface* s;
	struct imgpriv* p;


	if ((0 == path) || (0 == img)) {
		errno = EINVAL;
		return -1;
	}


	gprintf("gfx_loadimg: Loading '%s'\n", path);


	if (0 == (p = img->priv = calloc(1, sizeof(*p)))) {
		return -1;
	}

	if (0 == (s = IMG_Load(path))) {
		eprintf("Failed to load image: %s\n", IMG_GetError());
		free(p);
		img->priv = 0;
		return -1;
	}

	gprintf("surface (%ux%u)\n", s->w, s->h);
	p->fwidth  = (s->w / ncols);
	p->fheight = (s->h / nrows);


	if (0 != (p->texture = SDL_CreateTextureFromSurface(rendrr, s))) {
		img->ncols = ncols;
		img->nrows = nrows;
		img->nimgs = nimgs;
	} else {
		free(p);
		p = 0;
		img->priv = 0;
	}

	SDL_FreeSurface(s);

	return -(0 == p);
}

static int gfx_unloadimg(struct img* img)
{
	struct imgpriv* p;

	if (0 == img) {
		errno = EINVAL;
		return -1;
	} else if (0 != img->priv) {
		p = img->priv;
		SDL_DestroyTexture(p->texture);
		free(p);
		img->priv = 0;
	}

	return 0;
}

/* ------------------------------------------------------------------ */

static uint64_t now(void)
{
	struct timeval tv;

	gettimeofday(&tv, 0);

	return ((((uint64_t)tv.tv_sec) * 1000000) + tv.tv_usec);
}

static int gfx_getkey(int timeout)
{
	assert(0 != rendrr);

	uint64_t a;
	uint64_t d;
	SDL_Event ev;

	a = now();

	do {
		SDL_PollEvent(&ev);
		if (SDL_KEYDOWN == ev.type) {
			return ev.key.keysym.sym;
		}
		d = ((now() - a) / 1000);
	} while ((0 <= timeout) && (d < timeout));


	return 0;
}

/* ------------------------------------------------------------------ */

static SDL_Window* win;

static int init_gfx(void)
{
	assert(0 == win);

	if (0 != SDL_Init(SDL_INIT_VIDEO)) {
		xeprintf(2, "Unable to initialize SDL: %s\n", SDL_GetError());
	}

	if (0 == (win = SDL_CreateWindow("tp3",
	             SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
	             WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_FLAGS)))
	{
		xeprintf(2, "Unable to create SDL window: %s\n", SDL_GetError());
	}

	if (0 == (rendrr = SDL_CreateRenderer(win, -1, RENDERER_FLAGS))) {
		xeprintf(2, "Unable to create SDL renderer: %s\n", SDL_GetError());
	}

	if (0 != SDL_SetRenderDrawColor(rendrr, 0, 0, 0, -1)) {
		xeprintf(2, "Failed to set SDL drawing color: %s\n", SDL_GetError());
	}

	if (0 != TTF_Init()) {
		xeprintf(2, "Failed to to load SDL/TTF: %s\n", SDL_GetError());
	}

	if (IMG_INIT_PNG != IMG_Init(IMG_INIT_PNG)) {
		xeprintf(2, "Failed to load SDL/IMG: %s\n", IMG_GetError());
	}

	SDL_ShowWindow(win);

	return 0;
}

static void exit_gfx(void)
{
	if (0 != win) {
		SDL_DestroyWindow(win);
		win = 0;
	}

	if (0 != rendrr) {
		SDL_DestroyRenderer(rendrr);
		rendrr = 0;
	}

	if (0 != font) {
		TTF_CloseFont(font);
	}

	if (0 != TTF_WasInit()) {
		TTF_Quit();
	}

	IMG_Quit();
	SDL_Quit();
}

/* ------------------------------------------------------------------ */

const struct ui gfxui = {
	.init		= init_gfx,
	.exit		= exit_gfx,
	.load		= gfx_load,
	.unload		= gfx_unload,
	.update		= gfx_update,
	.write		= gfx_write,
	.draw		= gfx_draw,
	.drawoff	= gfx_drawoff,
	.clearoff	= gfx_clearoff,
	.clear		= gfx_clear,
	.loadimg	= gfx_loadimg,
	.unloadimg	= gfx_unloadimg,
	.getkey		= gfx_getkey
};

/* ================================================================== */
