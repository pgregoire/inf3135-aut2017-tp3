/*
 * Copyright 2017, Philippe Gregoire <pg@pgregoire.xyz>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef UI_UI_H
#define UI_UI_H
/* ================================================================== */

#include "../ui.h"

/* ------------------------------------------------------------------ */

struct screen {
	size_t	width;
	size_t	height;
	char*	matrix;
};

/**
 * screen_make(width, height, separators, scr)
 *
 * Create a screen out of a @width by @height grid
 * with the given @separators and store the result
 * into @scr.
 */
extern int screen_make(size_t, size_t, const char*, struct screen*);

/**
 * screen_destroy(screen)
 *
 * Destroy a previously created @screen.
 */
extern void screen_destroy(struct screen*);

/**
 * screen_lenfor(size)
 *
 * Return the length of a one-dimensional matrix
 * with @size cells in its grid.
 */
extern size_t screen_lenfor(size_t);

/**
 * screen_mindexof(x, y, width)
 *
 * Return the index of a matrix cell given
 * its grid's (@x, @y) coordinates and @width.
 */
extern size_t screen_mindexof(size_t, size_t, size_t);

/* ------------------------------------------------------------------ */

/*
 * The UI-implementing module interface.
 *
 * init:      initialize the module
 * exit:      de-initialize the module
 * load:      load and draw a screen
 * unload:    clear the screen
 * update:    repaint the screen
 * clear:     clear a cell
 * write:     write a string in a cell
 * draw:      draw an image in a cell
 * clearoff:  clear an offseted image
 * move:      move an image from a cell to another cell
 * loadimg:   load an image
 * unloadimg: unload an image
 * getkey:    get a single key press
 */
struct ui {
	int	(*init)(void);
	void	(*exit)(void);

	int	(*load)(const struct screen*);
	void	(*unload)(void);
	int	(*update)(void);

	int	(*write)(const char*, const struct coord*);
	int	(*draw)(size_t, const struct img*, const struct coord*);
	int	(*drawoff)(size_t, const struct img*, double, double,
		        const struct coord*, const struct coord*);
	int	(*clearoff)(const struct img*, double, double,
		            const struct coord*, const struct coord*);
	int	(*clear)(const struct coord*);

	int	(*loadimg)(const char*, size_t, size_t, size_t, struct img*);
	int	(*unloadimg)(struct img*);

	int	(*getkey)(int);
};

/* ------------------------------------------------------------------ */

#undef uimod

#ifdef USE_TUI
# warning The TUI is not implemented. Falling back to default UI.
#endif

#if (USE_GUI || !defined(uimod))
extern const struct ui gfxui;
# define uimod gfxui
#endif

#ifndef uimod
# error Failed to assign a UI-implementing module.
#endif /* uimod */

/* ================================================================== */
#endif /* UI_UI_H */
