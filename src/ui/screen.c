/*
 * Copyright 2017, Philippe Gregoire <pg@pgregoire.xyz>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* ================================================================== */

#include <assert.h>
#include <errno.h>
#include <stdlib.h>

#include "ui.h"
#include "../utils.h"

/* ------------------------------------------------------------------ */
/*
 * The screen is treated as a matrix where elements set
 * must be filled.
 */

size_t screen_lenfor(size_t ncells)
{
	/* separator + cell + [...] + separator */
	return ((2 * ncells) + 1);
}

size_t screen_mindexof(size_t x, size_t y, size_t width)
{
	return ((y * screen_lenfor(width)) + x);
}

static void setsep(size_t x, size_t y, struct screen* scr)
{
	assert((0 != scr) && (0 != scr->matrix));

	scr->matrix[screen_mindexof(x, y, scr->width)] = 1;
}

static int
setseps(size_t x, size_t y, size_t i, const char* seps, struct screen* scr)
{
	assert((0 != seps) && (0 != scr));

	size_t d;
	size_t ms;
	size_t mx;
	size_t my;


	mx = screen_lenfor(x);
	my = screen_lenfor(y);

	for (d = 0 ; 4 > d ; d++) {
		ms = (1 << d);
		if (0 == (seps[i] & ms)) {
			continue;
		}

		switch (ms) {
		case SEP_UP:
			/* top left, top , top right */
			setsep((mx - 1), (my - 1), scr);
			setsep( mx     , (my - 1), scr);
			setsep((mx + 1), (my - 1), scr);
			break;
		case SEP_RIGHT:
			/* top right, right, bottom right */
			setsep((mx + 1), (my - 1), scr);
			setsep((mx + 1),  my     , scr);
			setsep((mx + 1), (my + 1), scr);
			break;
		case SEP_DOWN:
			/* bottom right, bottom, bottom left */
			setsep((mx + 1), (my + 1), scr);
			setsep( mx     , (my + 1), scr);
			setsep((mx - 1), (my + 1), scr);
			break;
		case SEP_LEFT:
			/* bottom left, left, top left */
			setsep((mx - 1), (my + 1), scr);
			setsep((mx - 1),  my     , scr);
			setsep((mx - 1), (my - 1), scr);
			break;
		default:
			xeprintf(2, "ui/setseps: internal error\n");
			break;
		}
	}

	return 0;
}

static int make(const char* seps, struct screen* scr)
{
	assert((0 != seps) && (0 != scr) && (0 != scr->matrix));

	size_t i;
	size_t x;
	size_t y;

	for (y = 0 ; y < scr->height ; y++) {
		for (x = 0 ; x < scr->width ; x++) {
			i = ((y * scr->width) + x);
			if (0 != seps[i]) {
				setseps(x, y, i, seps, scr);
			}
		}
	}

	return 0;
}

int
screen_make(size_t width, size_t height, const char* seps, struct screen* scr)
{
	assert((0 != seps) && (0 != scr));

	int e;
	size_t h;
	size_t w;

	w = screen_lenfor(width);
	h = screen_lenfor(height);

	if (0 == (scr->matrix = calloc((w * h), sizeof(scr->matrix[0])))) {
		return -1;
	}

	scr->width = width;
	scr->height = height;

	if (0 == make(seps, scr)) {
		return 0;
	}

	e = errno;
	free(scr->matrix);
	scr->matrix = 0;
	errno = e;

	return -1;
}

/* ------------------------------------------------------------------ */

void screen_destroy(struct screen* scr)
{
	if ((0 != scr) && (0 != scr->matrix)) {
		free(scr->matrix);
		scr->matrix = 0;
	}
}

/* ================================================================== */
