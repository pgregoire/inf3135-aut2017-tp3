/*
 * Copyright 2017, Philippe Gregoire <pg@pgregoire.xyz>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* A simple wrapper around the UI-implementing module. */

/* ================================================================== */

#include <errno.h>

#include "ui.h"
#include "../utils.h"

/* ------------------------------------------------------------------ */

static struct screen scr;

int ui_load(size_t width, size_t height, const char* seps)
{
	if (0 == seps) {
		errno = EINVAL;
		return -1;
	}

	if (0 != screen_make(width, height, seps, &scr)) {
		return -1;
	}

	return uimod.load(&scr);
}

void ui_unload(void)
{
	uimod.unload();
	screen_destroy(&scr);
}

int ui_update(void)
{
	return uimod.update();
}

/* ------------------------------------------------------------------ */

int ui_write(const char* str, const struct coord* coord)
{
	return uimod.write(str, coord);
}

int
ui_draw(size_t frame, const struct img* img, const struct coord* coord)
{
	return uimod.draw(frame, img, coord);
}

int
ui_drawoff(size_t frame, const struct img* img,
           double xoff, double yoff,
           const struct coord* from, const struct coord* to)
{
	return uimod.drawoff(frame, img, xoff, yoff, from, to);
}

int ui_clearoff(const struct img* img, double xoff, double yoff,
                const struct coord* from, const struct coord* to)
{
	return uimod.clearoff(img, xoff, yoff, from, to);
}

int ui_clear(const struct coord* coord)
{
	return uimod.clear(coord);
}

/* ------------------------------------------------------------------ */

int ui_loadimg(const char* path, size_t ncols, size_t nrows, size_t nimgs,
               struct img* img)
{
	return uimod.loadimg(path, ncols, nrows, nimgs, img);
}

int ui_unloadimg(struct img* img)
{
	return uimod.unloadimg(img);
}

/* ------------------------------------------------------------------ */

int ui_getkey(int timeout)
{
	return uimod.getkey(timeout);
}

/* ------------------------------------------------------------------ */

int init_ui(void)
{
	return uimod.init();
}

void exit_ui(void)
{
	screen_destroy(&scr);

	uimod.exit();
}

/* ================================================================== */
