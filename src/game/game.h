/*
 * Copyright 2017, Philippe Gregoire <pg@pgregoire.xyz>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef GAME_GAME_H
#define GAME_GAME_H
/* ================================================================== */

#include "../game.h"
#include "../ui.h"
#include "../utils.h"

/* ------------------------------------------------------------------ */

/* game grid */
#define GRID_UP		0
#define GRID_RIGHT	1
#define GRID_DOWN	2
#define GRID_LEFT	3

/**
 * The screen is formatted like a grid in which
 * elements can be drawn.
 *
 * This structure holds the screen's grid structure.
 */
struct grid {
	size_t	width;
	size_t	height;
	char*	seps;
	char*	donuts;
};

/**
 * grid_load(name, grid)
 *
 * Load a grid named @name into @grid.
 *
 * If @grid already contains a grid, its
 * content is erased and lost.
 */
extern int grid_load(const char*, struct grid*);

/**
 * grid_unload(grid)
 *
 * Destroy a grid and its associated resources.
 */
extern void grid_unload(struct grid*);

/**
 * grid_hassep(x, y, d, grid)
 */
extern int grid_hassep(size_t, size_t, size_t, const struct grid*);

/**
 * grid_canmove(x, y, dir, grid)
 *
 * Returns non-zero if an object at (@x, @y) can
 * move in direction @dir inside a given @grid.
 */
extern int grid_canmove(size_t, size_t, size_t, const struct grid*);

/* ------------------------------------------------------------------ */

/* game states */
#define GAME_INVAL	(-1)
#define GAME_READY	(-2)
#define GAME_QUIT	(-3)
#define GAME_MENU	0
#define GAME_PLAY	1
#define GAME_END	2

struct gamemode {
	int	(*init)(void);
	void	(*exit)(void);
	int	(*run)(void);
};


extern const struct gamemode gamemode_menu;
extern const struct gamemode gamemode_play;
extern const struct gamemode gamemode_end;

static const struct gamemode* const gamemodes[] = {
	[GAME_MENU] = &gamemode_menu,
	[GAME_PLAY] = &gamemode_play,
	[GAME_END]  = &gamemode_end
};

/* ================================================================== */
#endif /* GAME_GAME_H */
