/*
 * Copyright 2017, Philippe Gregoire <pg@pgregoire.xyz>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* ==================================================================== */

#include <assert.h>
#include <string.h>

#include "game.h"

/* -------------------------------------------------------------------- */

static size_t state = GAME_INVAL;

/* -------------------------------------------------------------------- */

int game_run(void)
{
	gprintf("Running game module\n");

	assert(GAME_READY == state);

	state = GAME_MENU;

	while (GAME_QUIT != state) {
		assert(NARRAY(gamemodes) > state);
		state = gamemodes[state]->run();
	}

	state = GAME_QUIT;

	return 0;
}

int init_game(void)
{
	gprintf("Initializing game module\n");

	assert(GAME_INVAL == state);

	size_t i;

	for (i = 0 ; NARRAY(gamemodes) > i ; i++) {
		if (0 != gamemodes[i]->init) {
			if (0 != gamemodes[i]->init()) {
				return -1;
			}
		}
	}

	state = GAME_READY;

	return 0;
}

void exit_game(void)
{
	gprintf("Exiting game module\n");

	size_t i;

	for (i = 0 ; NARRAY(gamemodes) > i ; i++) {
		if (0 != gamemodes[i]->exit) {
			gamemodes[i]->exit();
		}
	}

	state = GAME_INVAL;
}

/* ==================================================================== */
