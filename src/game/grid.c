/*
 * Copyright 2017, Philippe Gregoire <pg@pgregoire.xyz>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* ================================================================== */

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <jansson.h>

#include "game.h"
#include "../utils.h"

/* ------------------------------------------------------------------ */

#define UP	(1 << GRID_UP)
#define RIGHT	(1 << GRID_RIGHT)
#define DOWN	(1 << GRID_DOWN)
#define LEFT	(1 << GRID_LEFT)

/* ------------------------------------------------------------------ */

static size_t nseps(const struct grid* grid)
{
	assert(0 != grid);

	size_t w;
	size_t h;

	w = grid->width;
	h = grid->height;

	return ( (w * (w - 1)) + (h * (h - 1)) + (2 * w) + (2 * h) );
}

static size_t ncells(const struct grid* grid)
{
	return (grid->width * grid->height);
}

/* ------------------------------------------------------------------ */
/* (un)loading */

static int parse_dimensions(json_t* js, struct grid* grid)
{
	assert(0 != js);

	size_t i;
	size_t v;
	json_t* j;


	if ( (0 == json_is_array(js)) ||
	     (2 != json_array_size(js)) )
	{
		errno = EINVAL;
		return -1;
	}


	grid->width  = 0;
	grid->height = 0;

	json_array_foreach(js, i, j) {
		if ( (0 == json_is_integer(j)) ||
		     (0 >= json_number_value(j)) )
		{
			errno = EINVAL;
			return -1;
		}

		v = (size_t)json_number_value(j);

		if (0 == grid->width) {
			grid->width = v;
		} else {
			grid->height = v;
		}
	}

	return -((0 == grid->width) || (0 == grid->height));
}

static int parse_separator(json_t* js, size_t idx, struct grid* grid)
{
	assert((0 != js) && (0 != grid) && (0 != grid->seps));

	size_t i;
	json_t* j;


	if ( (0 == json_is_array(js)) ||
	     (4 != json_array_size(js)) )
	{
		errno = EINVAL;
		return -1;
	}

	json_array_foreach(js, i, j) {
		if (0 == json_is_integer(j)) {
			errno = EINVAL;
			return -1;
		}

		grid->seps[idx] |= (0 != json_number_value(j)) << i;
	}

	return 0;
}

static int parse_separators(json_t* js, struct grid* grid)
{
	assert((0 != js) && (0 != grid));

	size_t i;
	json_t* j;


	if ( (0 == json_is_array(js)) ||
	     (nseps(grid) < json_array_size(js)) )
	{
		errno = EINVAL;
		return -1;
	}


	grid->seps = calloc(sizeof(*grid->seps), nseps(grid));
	if (0 == grid->seps) {
		return -1;
	}

	json_array_foreach(js, i, j) {
		if (0 != parse_separator(j, i, grid)) {
			return -1;
		}
	}

	return 0;
}

static int parse_donuts(json_t* js, struct grid* grid)
{
	assert((0 != js) && (0 != grid));

	size_t i;
	size_t v;
	json_t* j;


	if ( (0 == json_is_array(js)) ||
	     (nseps(grid) <= json_array_size(js)) )
	{
		errno = EINVAL;
		return -1;
	}


	grid->donuts = calloc(sizeof(*grid->donuts), ncells(grid));
	if (0 == grid->donuts) {
		return -1;
	}

	json_array_foreach(js, i, j) {
		if ( (0 == json_is_integer(j)) ||
		     (nseps(grid) <= json_number_value(j)) )
		{
			errno = EINVAL;
			return -1;
		}

		v = (size_t)json_number_value(j);
		/* could be a bitfield, but too complex for the gain */
		grid->donuts[v] = 1;
	}

	return 0;
}

static int parse_grid(json_t* js, struct grid* grid)
{
	assert((0 != js) && (0 != grid));

	json_t* v;
	const char* k;


	if ( (0 == json_is_object(js)) ||
	     (3 != json_object_size(js)) )
	{
		errno = EINVAL;
		return -1;
	}


	memset(grid, 0, sizeof(*grid));

	json_object_foreach(js, k, v) {
		if (0 == strcmp("dimensions", k)) {
			if (0 != parse_dimensions(v, grid)) {
				return -1;
			}
		} else if (0 == strcmp("separators", k)) {
			if (0 != parse_separators(v, grid)) {
				return -1;
			}
		} else if (0 == strcmp("donuts", k)) {
			if (0 != parse_donuts(v, grid)) {
				return -1;
			}
		} else {
			eprintf("grid: unknown key: %s\n", k);
			errno = EINVAL;
			return -1;
		}
	}

	return 0;
}

int grid_load(const char* name, struct grid* grid)
{
	int e;
	int r;
	json_t* js;
	json_error_t err;
	char buf[PATH_MAX];


	if ((0 == name) || (0 == grid)) {
		errno = EINVAL;
		return -1;
	}


	snprintf(buf, sizeof(buf), "src/assets/grids/%s.json", name);

	if (0 == (js = json_load_file(buf, 0, &err))) {
		eprintf("grid: %s\n", err.text);
		return -1;
	}


	r = parse_grid(js, grid);
	e = errno;
	json_decref(js);

	errno = e;
	return r;
}

void grid_unload(struct grid* grid)
{
	if (0 != grid) {
		if (0 != grid->seps) {
			free(grid->seps);
		}
		if (0 != grid->donuts) {
			free(grid->donuts);
		}

		memset(grid, 0, sizeof(*grid));
	}
}

/* ------------------------------------------------------------------ */

int grid_hassep(size_t x, size_t y, size_t d, const struct grid* grid)
{
	size_t i;


	i = ((y * grid->width) + x);

	if ( (0 == grid) ||
	     (4 <= d) ||
	     (i >= (grid->height * grid->width)) )
	{
		errno = EINVAL;
		return -1;
	}

	if ( (0 == grid->seps) ||
	     (0 != (grid->seps[i] & (1 << d))) )
	{
		return 1;
	}

	return 0;
}

/* ------------------------------------------------------------------ */

int grid_canmove(size_t x, size_t y, size_t dir, const struct grid* grid)
{
	assert( (0 != grid) && (0 != grid->seps) &&
	        (x < grid->width) && (y < grid->height) &&
	        (4 > dir) );

	return 0 == (grid->seps[((y * grid->width) + x)] & (1 << dir));
}

/* ------------------------------------------------------------------ */

#ifdef GRID_MAIN
int main(void)
{
	struct grid g;

	grid_load("menu", &g); grid_unload(&g);
	grid_load("level001", &g); grid_unload(&g);

	return 0;
}
#endif

/* ================================================================== */
