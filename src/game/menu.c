/*
 * Copyright 2017, Philippe Gregoire <pg@pgregoire.xyz>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* ================================================================== */

#include <assert.h>
#include <stdio.h>

#include "game.h"

/* ------------------------------------------------------------------ */

#define KEY_PLAY	'1'
#define KEY_QUIT	'0'

/* ------------------------------------------------------------------ */

static struct grid grid;

/* ------------------------------------------------------------------ */

static int draw_menu(void)
{
	struct coord c;

	ui_load(grid.width, grid.height, grid.seps);

	c.x = 0;
	c.y = 1;
	ui_clear(&c);
	ui_write("Super Bob World", &c);

	c.x = 0;
	c.y = 4;
	ui_write("Press 1 to play", &c);

	c.y++;
	ui_write("Press 0 to quit", &c);

	ui_update();

	return 0;
}

static int menu_run(void)
{
	int ch;

	gprintf("Running game/menu module\n");

	if (0 != draw_menu()) {
		return -1;
	}

	while (0 != 1) {
		ch = ui_getkey(-1);
		switch (ch) {
		case KEY_QUIT:
			return GAME_QUIT;
		case KEY_PLAY:
			return GAME_PLAY;
		default:
			/* ignore */
			break;
		}
	}

	/* should never be reached */
	xeprintf(127, "menu_run: reached the unreachable\n");

	return GAME_QUIT;
}

/* ------------------------------------------------------------------ */

static int init_menu(void)
{
	gprintf("Initializing game/menu module\n");

	if (0 != grid_load("menu", &grid)) {
		return -1;
	}

	return 0;
}

static void exit_menu(void)
{
	gprintf("Exiting game/menu\n");

	grid_unload(&grid);
}

/* ------------------------------------------------------------------ */

const struct gamemode gamemode_menu = {
	.init	= init_menu,
	.exit	= exit_menu,
	.run	= menu_run
};

/* ================================================================== */
