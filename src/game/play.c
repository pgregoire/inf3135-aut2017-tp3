/*
 * Copyright 2017, Philippe Gregoire <pg@pgregoire.xyz>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* ==================================================================== */

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "game.h"

/* -------------------------------------------------------------------- */

#define CHR_JUMP	0x80

#define KEY_LEFT	'a'
#define KEY_RIGHT	'd'
#define KEY_JUMP	'w'
#define KEY_FRONT	's'
#define KEY_QUIT	'q'

#define DRAW_GRID	(1 << 0)
#define DRAW_CHR	(1 << 1)
#define DRAW_DONUTS	(1 << 2)
#define DRAW_ALL	(-1)

/* the timeout between screen refreshes, in milliseconds */
#define WAITTIMEOUT	(1000 / 64)

/* -------------------------------------------------------------------- */

static const struct {
	char*	name;
	size_t	ncols;
	size_t	nrows;
	size_t	nimgs;
} imgmeta[] = {
	{ "character",  32, 6, (32 * 6) },
	{ "donut",      32, 1, (32 * 1) }
};

#define IMG_CHR		0
#define IMG_DONUT	1

static struct grid grid;
static struct img imgs[NARRAY(imgmeta)];

static size_t cdir;
static struct coord ccoord;
static size_t ndonuts;

/* -------------------------------------------------------------------- */
/* image management */

static int loadimg(size_t i, struct img* img)
{
	assert((i < NARRAY(imgmeta)) && (0 != img));

	char buf[PATH_MAX];

	snprintf(buf, sizeof(buf), "src/assets/%s.png", imgmeta[i].name);

	return ui_loadimg(buf, imgmeta[i].ncols, imgmeta[i].nrows,
	                  imgmeta[i].nimgs, img);
}

static int loadimgs(void)
{
	size_t i;

	for (i = 0 ; NARRAY(imgs) > i ; i++) {
		if (0 != loadimg(i, &imgs[i])) {
			return -1;
		}
	}

	return 0;
}

/* -------------------------------------------------------------------- */
/* donuts management */

static inline int hasdonut(size_t x, size_t y)
{
	return grid.donuts[((y * grid.width) + x)];
}

static inline void takedonutat(const struct coord* c)
{
	if (0 != hasdonut(c->x, c->y)) {
		ui_clear(c);
		grid.donuts[((c->y * grid.width) + c->x)] = 0;
		ndonuts--;
	}
}

static inline void takedonut(void)
{
	takedonutat(&ccoord);
}

static int draw_donuts(void)
{
	struct coord c;
	static size_t f;


	if (imgmeta[IMG_DONUT].ncols <= f) {
		f = 0;
	}
	gprintf("Drawing donut frame %lu/%lu\n", f, imgmeta[IMG_DONUT].ncols);

	for (c.y = 0 ; grid.height > c.y ; c.y++) {
		for (c.x = 0 ; grid.height > c.x ; c.x++) {
			if (0 != hasdonut(c.x, c.y)) {
				ui_clear(&c);
				ui_draw(f, &imgs[IMG_DONUT], &c);
			}
		}
	}
	f++;

	return 0;
}

/* -------------------------------------------------------------------- */
/* character management */

static size_t cdir2row(int jump)
{
	if (0 == jump) {
		switch (cdir) {
		case GRID_LEFT:  return 1;
		case GRID_RIGHT: return 0;
		default:         return 2;
		}
	}

	switch (cdir) {
	case GRID_LEFT:  return 5;
	case GRID_RIGHT: return 4;
	default:         break;
	}

	return 3;
}

static int draw_character(void)
{
	ui_clear(&ccoord);
	ui_draw((cdir2row(0) * imgmeta[IMG_CHR].ncols),
                &imgs[IMG_CHR], &ccoord);

	return 0;
}

/* ------------------------------------------------------------------ */

static void redraw(size_t mask, int update)
{
	if (0 != (mask & DRAW_GRID)) {
		ui_load(grid.width, grid.height, grid.seps);
	}
	if (0 != (mask & DRAW_DONUTS)) {
		draw_donuts();
	}
	if (0 != (mask & DRAW_CHR)) {
		draw_character();
	}

	if (0 != update) {
		ui_update();
	}
}

/* ------------------------------------------------------------------ */
/* movements */

static inline int canmoveab(size_t x, size_t y, int dir)
{
	return grid_canmove(x, y, dir, &grid);
}

static inline int canmove(int dir)
{
	return canmoveab(ccoord.x, ccoord.y, dir);
}

static inline int getframe(size_t i, int jump)
{
	return ((cdir2row(jump) * imgs[IMG_CHR].ncols) + i);
}

static int __move(const struct coord* to)
{
	assert(0 != to);

	size_t f;
	double xo;
	double yo;
	size_t ncols;

	xo = 1;
	yo = 1;
	ncols = imgs[IMG_CHR].ncols;

	for (f = 0 ; ncols > f ; f++) {
		if (to->y == ccoord.y) {
			xo = (1 - (((double)(ncols - f)) / ncols));
		} else {
			yo = (1 - (((double)(ncols - f)) / ncols));
		}

		ui_clearoff(&imgs[IMG_CHR], xo, yo, &ccoord, to);
		redraw(DRAW_DONUTS, 0);
		ui_drawoff(getframe(f, 0), &imgs[IMG_CHR], xo, yo, &ccoord, to);
		redraw(0, 1);
		usleep(1000 * WAITTIMEOUT);
	}

	return 0;
}

static int move(int dir)
{
	struct coord ncoord;

	if ((GRID_DOWN == dir) || (dir == cdir)) {
		if (0 == canmove(dir)) {
			return 0;
		}

		switch (dir) {
		case GRID_RIGHT:
			ncoord.x = (ccoord.x + 1);
			ncoord.y = ccoord.y;
			break;
		case GRID_LEFT:
			ncoord.x = (ccoord.x - 1);
			ncoord.y = ccoord.y;
			break;
		case GRID_DOWN:
			ncoord.x = ccoord.x;
			ncoord.y = (ccoord.y + 1);
			break;
		case GRID_UP:
			return 0;
		}

		if (0 != __move(&ncoord)) {
			return -1;
		}

		ccoord.x = ncoord.x;
		ccoord.y = ncoord.y;


		takedonut();
		return move(GRID_DOWN);
	} else {
		gprintf("Orienting\n");
		cdir = dir;
	}

	return 0;
}

static inline double jumpy(double x)
{
	/* http://www.analyzemath.com/parabola/three_points_para_calc.html */
	return (-(3.452*x*x) + (4.452*x));
}

static int __jump(const struct coord* to)
{
	assert(0 != to);

	size_t f;
	double xo;
	double yo;
	size_t ncols;

	ncols = imgs[IMG_CHR].ncols;

	for (f = 0 ; ncols > f ; f++) {
		xo = (1 - (((double)(ncols - f)) / ncols));
		yo = jumpy(xo);
		ui_clearoff(&imgs[IMG_CHR], xo, yo, &ccoord, to);
		redraw((DRAW_GRID | DRAW_DONUTS), 0);
		ui_drawoff(getframe(f, 1), &imgs[IMG_CHR], xo, yo, &ccoord, to);
		redraw(0, 1);
		usleep(1000 * WAITTIMEOUT);
	}

	ccoord.x = to->x;
	ccoord.y = to->y;

	redraw(DRAW_ALL, 1);

	return 0;
}

static int jump(int dir)
{
	struct coord c;

	if (0 == canmoveab(ccoord.x, ccoord.y, GRID_UP)) {
		return 0;
	}

	c.x = ccoord.x;
	c.y = (ccoord.y - 1);
	if (GRID_UP != dir) {
		if (0 == canmoveab(c.x, c.y, dir)) {
			return 0;
		}
	}
	takedonutat(&c);


	switch (dir) {
	case GRID_LEFT:
		c.x = (ccoord.x - 1);
		break;
	case GRID_RIGHT:
		c.x = (ccoord.x + 1);
		break;
	default:
		break;
	}

	if (0 != __jump(&c)) {
		return -1;
	}

	takedonut();
	return move(GRID_DOWN);
}

/* ------------------------------------------------------------------ */

static int play(void)
{
	int ch;

	ccoord.x = 0;
	ccoord.y = (grid.height - 1);

	ch = 0;
	cdir = GRID_UP;

	while (1) {
		if (0 == ndonuts) {
			return GAME_END;
		}

		while (0 == ch) {
			redraw((DRAW_CHR | DRAW_DONUTS), 1);
			ch = ui_getkey(WAITTIMEOUT);
		}

		switch (ch) {
		case KEY_QUIT:
			return GAME_QUIT;
		case KEY_FRONT:
			cdir = GRID_UP;
			break;
		case KEY_JUMP:
			jump(cdir);
			break;
		case KEY_LEFT:
			move(GRID_LEFT);
			break;
		case KEY_RIGHT:
			move(GRID_RIGHT);
			break;
		default:
			/* ignore */
			break;
		}

		while (0 != ch) {
			ch = ui_getkey(1);
		}
	}

	/* never reached */
	xeprintf(2, "game/play: internal error (play)\n");
	return GAME_QUIT;
}

static int play_run(void)
{
	if (0 != ui_load(grid.width, grid.height, grid.seps)) {
		return -1;
	}

	return play();
}

/* -------------------------------------------------------------------- */

static int init_play(void)
{
	gprintf("Entering game/play\n");

	size_t i;

	/* TODO Detect levels at run-time */
	if (0 != grid_load("level001", &grid)) {
		return -1;
	}

	for (i = 0 ; (grid.width * grid.height) > i ; i++) {
		if (0 != grid.donuts[i]) {
			ndonuts++;
		}
	}

	if (0 != loadimgs()) {
		return -1;
	}

	return 0;
}

static void exit_play(void)
{
	gprintf("Exiting game/play\n");
	grid_unload(&grid);
}

/* -------------------------------------------------------------------- */

const struct gamemode gamemode_play = {
	.init	= init_play,
	.exit	= exit_play,
	.run	= play_run
};

/* ==================================================================== */
