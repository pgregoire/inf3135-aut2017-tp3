/*
 * Copyright 2017, Philippe Gregoire <pg@pgregoire.xyz>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef UI_H
#define UI_H
/* ================================================================== */

#include <stddef.h>

/* ------------------------------------------------------------------ */

#define SEP_UP		(1 << 0)
#define SEP_RIGHT	(1 << 1)
#define SEP_DOWN	(1 << 2)
#define SEP_LEFT	(1 << 3)

struct coord {
	size_t	x;
	size_t	y;
};

struct img {
	size_t	ncols;
	size_t	nrows;
	size_t	nimgs;

	void*	priv;
};

/* ------------------------------------------------------------------ */

/**
 * ui_load(width, height, separators)
 *
 * Load a screen composed of @width by @height
 * cells and the given @separators.
 *
 * @separators must be an array of length
 * (@width * @height) where each element
 * represents the cell's surrounding separators
 * via an OR'ed result of SEP_* values.
 */
extern int ui_load(size_t, size_t, const char*);

/**
 * ui_unload()
 *
 * Unload the grid.
 */
extern void ui_unload(void);

/**
 * ui_write(str, coord)
 *
 * Write @str at @coord.
 */
extern int ui_write(const char*, const struct coord*);

/**
 * ui_draw(frame, img, coord)
 *
 * Draw the frame @frame of @img at @coord.
 */
extern int ui_draw(size_t, const struct img*, const struct coord*);

/**
 * ui_clear(coord)
 *
 * Clear the grid at @coord.
 */
extern int ui_clear(const struct coord*);

/**
 * ui_drawoff(frame, image, xoff, yoff, from, to)
 *
 * Draw an @image @frame at between @from and @to.
 * The exact coordinates where the image is drawn
 * is derived from @xoff and @yoff.
 *
 * Specifically, given a straight line between
 * @from and @to, and where a full "move" represents
 * the offsets pair (100,100), @xoff represents the
 * percentage by which (@from.x - @to.x) is multiplied
 * to compute on which x coordinate the image must be
 * drawn. Idem for @yoff.
 *
 * For example, if @xoff and @yoff are 50. The image
 * would be drawn halfway between @from and @to.
 */
extern int ui_drawoff(size_t, const struct img*,
                      double, double,
                      const struct coord*, const struct coord*);

/**
 * ui_clearoff(img, xoff, yoff, from, to)
 *
 * Clear an offseted-image.
 *
 * See ui_drawoff.
 */
extern int ui_clearoff(const struct img*, double, double,
                       const struct coord*, const struct coord*);

/**
 * ui_update()
 *
 * Update the screen's content.
 */
extern int ui_update(void);

/* ------------------------------------------------------------------ */

/**
 * ui_loadimg(path, ncols, nrows, nimgs, img)
 *
 * Load an image at @path and treat it as a spritesheet
 * of @ncols, @nrows containing @nimgs. The result is
 * stored in @img.
 */
extern int ui_loadimg(const char*, size_t, size_t, size_t, struct img*);

/**
 * ui_unloadimg(img)
 *
 * Destroy @img.
 */
extern int ui_unloadimg(struct img*);

/* ------------------------------------------------------------------ */

/**
 * ui_getkey(timeout)
 *
 * Wait for an input key up to @timeout milliseconds.
 */
extern int ui_getkey(int);

/* ------------------------------------------------------------------ */

/**
 * init_ui()
 *
 * Initialize the UI module.
 */
extern int init_ui(void);

/**
 * exit_ui()
 *
 * Terminate the UI module.
 */
extern void exit_ui(void);

/* ================================================================== */
#endif /* UI_H */
