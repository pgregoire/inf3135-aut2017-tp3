/*
 * Copyright 2017, Philippe Gregoire <pg@pgregoire.xyz>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef UTILS_H
#define UTILS_H
/* ================================================================== */

#include <stdarg.h>

/* ------------------------------------------------------------------ */

#define NARRAY(arr)	(sizeof(arr) / sizeof((arr)[0]))

/* ------------------------------------------------------------------ */

/**
 * void vgprintf(fmt, args)
 *
 * Print a formatted-string to stdout.
 *
 * The printed string is expanded from a printf-like
 * @fmt string using the values pointed by @args.
 *
 * NOTES
 * - If @fmt ends with ':', a space followed by the
 *   string associated with the value of errno is
 *   printed after the expanded content of @fmt.
 * - If @fmt does not end with a newline,
 *   one is printed.
 */
extern void vgprintf(const char*, va_list);

/**
 * void gprintf(fmt, ...)
 *
 * Print a formatted-string to stderr.
 *
 * The printed string is expanded from a printf-like
 * @fmt string; using the remaining arguments.
 *
 * See NOTES of vgprintf().
 */
extern void gprintf(const char*, ...);

/**
 * void veprintf(fmt, args)
 *
 * Print a formatted-string to stderr.
 *
 * The printed string is expanded from a printf-like
 * @fmt string using the values pointed by @args.
 *
 * See NOTES of vgprintf().
 */
extern void veprintf(const char*, va_list);

/**
 * void eprintf(fmt, ...)
 *
 * Print a formatted-string to stderr.
 *
 * The printed string is expanded from a printf-like
 * @fmt string; using the remaining arguments.
 *
 * See NOTES of vgprintf().
 */
extern void eprintf(const char*, ...);

/**
 * void xeprintf(xcode, fmt, ...)
 *
 * Print a formatted-string to stderr and exit.
 *
 * See NOTES of vgprintf().
 */
extern void xeprintf(int, const char*, ...);

/* ================================================================== */
#endif /* UTILS_H */
