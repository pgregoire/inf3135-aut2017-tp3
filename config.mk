

PREFIX=		/usr/local
BINDIR=		${PREFIX}/bin


CC=		gcc
LD=		${CC}

CPPFLAGS=	$$(pkg-config --cflags sdl2) -D_DEFAULT_SOURCE=
CFLAGS=		-Wall -Werror -pedantic -std=c99
LDFLAGS=	-Wl,-z,noexecstack -Wl,-z,now -Wl,-z,relro
LIBS=		$$(pkg-config --libs sdl2) $$(pkg-config --libs SDL2_image) \
		$$(pkg-config --libs SDL2_ttf) $$(pkg-config --libs jansson)
