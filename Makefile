#
.POSIX:

BIN=	tp3

all: ${BIN}


${BIN}: .${BIN}
	mv .${BIN} ${BIN}

.${BIN}:
	(cd src && ${MAKE} ${BIN};)
	cp src/${BIN} .${BIN}

check: ${BIN}
	bats tests/bats

ci:
	gitlab-ci-multi-runner exec docker test

clean:
	(cd src && ${MAKE} clean;)

distclean:
	(cd src && ${MAKE} distclean;)
