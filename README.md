# Travail pratique 3

Ce dépôt contient le code source du jeu vidéo *Super Bob World*.

Ce jeu vidéo a été programmé dans le cadre du cours INF3135 donné,
à l'automne 2017, à l'Université du Québec à Montréal.

Pour la description complète de l'énoncé veuillez visiter
[son dépôt](https://gitlab.com/ablondin/inf3135-aut2017-tp3-enonce).

## Compilation

### Pré-requis

Pour compiler le jeu, les éléments suivants sont requis :

* l'utilitaire `make`;
* un compilateur C conforme à la norme ISO/IEC 9899:1999;
* la librairie SDL2 et les sous-modules `gfx`, `image` et `ttf`;
* la librairie `jansson`.

N'oubliez surtout pas de récupérer les éléments graphiques en
effectuant les commandes :

```
git submodule init
git submodule update
```

### Comment compiler

Pour compiler le jeu, exécuter la commande :

```
make
```

## Comment jouer

Une fois le jeu compilé, il peut être lancé avec la commande :

```
./tp3
```

Pour avoir une trace de déboggage, l'option `-d` peut être passée
au programme.

### Déplacer le personnage

Pour déplacer le personnage, les touches suivantes sont disponibles :

* `a` -- orienter ou déplacer le personnage vers la gauche;
* `d` -- orienter ou déplacer le personnage vers la droite;
* `s` -- orienter le personnage vers l'avant;
* `w` -- faire sauter le personnage selon son orientation.

## Bogues et limitations

* Lorsque le personnage tombe d'une cellule à une autre, la force
  gravitationnelle est constante plutôt que d'être dépendante de la
  hauteur et la durée de la chute.
* Pour supporter le type d'interface terminal, la dynamique de mouvement
  est contrainte aux cellules de localisation. C'est-à-dire que le
  personnage ne peut bouger dans sa cellule et ne peut pas, par exemple,
  sauter jusqu'au plafond.
* Par économie de temps, le personnage est contraint à être localisé
  entièrement dans une cellule sauf durant l'animation des déplacements.
  Cette mécanique est identique au projet modèle (maze-sdl).

## Inspiration

L'implémentation du programme est vaguement inspiré de
[maze-sdl](https://bitbucket.org/ablondin-projects/maze-sdl/).

## License

Ce logiciel est distribué sous license GPL 3. Une copie de
la license est incluse avec le code source.
